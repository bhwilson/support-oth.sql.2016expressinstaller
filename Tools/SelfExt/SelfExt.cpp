/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Self Extractor - WinMain and Dialog's callback

#include "stdafx.h"
#include "Resource.h"
#include "ErrorStr.h"
#include "Worker.h"

//////////////////////////////////////////////////////////////////////
// Constants & prototypes

namespace chiclero
{
    const int WM_USER_PROGRESS = WM_USER + 0x100;

    BOOL CALLBACK FeedbackDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
    BOOL OnUserProgress(HWND hwnd, UINT uPos, UINT uRange);
}

//////////////////////////////////////////////////////////////////////
// Replacement startup code, so that it is not necessary to link with
// the run-time library

extern "C" void __cdecl WinMainCRTStartup(void) {
    ExitProcess(WinMain(GetModuleHandle(NULL), NULL, "", SW_SHOWDEFAULT));
}

#ifdef _MSC_VER // MS linker complains without this
extern "C" void __cdecl main(void) { }
#endif

namespace std {
    extern "C" void * __cdecl memset(void *s, int c, size_t n) {
        for (size_t i = 0; i < n; i++)
            ((char*)s)[i] = c;
        return s;
    }
}

//////////////////////////////////////////////////////////////////////
// WinMain

int WINAPI WinMain( HINSTANCE hInstance,
                    HINSTANCE /*hPrevInstance*/,
                    LPSTR     /*lpCmdLine*/,
                    int       /*nCmdShow*/ )
{
    using namespace chiclero;

    // init the common controls
    InitCommonControls();

    // create the feedback dialog
    HWND hwnd = CreateDialogParam(hInstance, MAKEINTRESOURCE(IDD_FEEDBACK), NULL, FeedbackDlgProc, 0);
    if (hwnd == NULL)
    {
        LoadOsError(IDS_ERR_DEFAULT);
        goto handle_error;
    }

    // run the worker code
    if (RunWorker(hwnd, WM_USER_PROGRESS))
        return TRUE;

handle_error:
    // a WM_QUIT in the queue will prevent the messagebox from being displayed
    MSG msg;
    PeekMessage(&msg, NULL, WM_QUIT, WM_QUIT, PM_REMOVE);
    TCHAR szErrTitle[32] = TEXT("Error");
    LoadString(hInstance, IDS_ERR_TITLE, szErrTitle, sizeof(szErrTitle) / sizeof(TCHAR));
    MessageBox(NULL, GetErrorStr(), szErrTitle, MB_OK | MB_ICONHAND | MB_SYSTEMMODAL);
    return FALSE;
}

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// DialogProc

BOOL CALLBACK FeedbackDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DESTROY:        PostQuitMessage(0); break;
    case WM_CLOSE:          DestroyWindow(hwnd); break;
    case WM_USER_PROGRESS:  OnUserProgress(hwnd, wParam, lParam); break;
    }

    return FALSE;
}

//////////////////////////////////////////////////////////////////////
// Progress

BOOL OnUserProgress(HWND hwnd, UINT uPos, UINT uRange)
{
    HWND hwndCtrl = GetDlgItem(hwnd, IDC_FEEDBACK_PROGRESS);
    if (uRange)
        SendMessage(hwndCtrl, PBM_SETRANGE, 0, MAKELPARAM(0, uRange));
    SendMessage(hwndCtrl, PBM_SETPOS, uPos, 0);
    return FALSE;
}

} // namespace
