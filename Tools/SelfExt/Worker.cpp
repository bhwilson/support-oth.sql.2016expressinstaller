/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Worker code, extracts the files and runs them

#include "stdafx.h"
#include "Resource.h"
#include "Worker.h"
#include "ErrorStr.h"
#include "PEMem.h"

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// pump messages, returns TRUE on WM_QUIT

BOOL IsCancelled() 
{ 
    MSG msg;
    msg.message = 0;
    static BOOL bCancel = FALSE;

    if (bCancel)
        return TRUE;

    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        bCancel = msg.message == WM_QUIT ||
            (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE);
        if (bCancel)
            return TRUE;

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return FALSE;
}

//////////////////////////////////////////////////////////////////////
// Make sure the directory for the given file exists, create if not.
// pszFilePath is the full path to a file, nTempDirLen is the length
// of the temp directory part of the filename, that can be assumed
// to exist.

void EnsureFileDirectoryExists(LPTSTR pszFilePath, int nTempDirLen)
{
    int nFilePathLen = lstrlen(pszFilePath);
    int i = nFilePathLen;

    // check parameters are valid
    if (nTempDirLen <= 0 || nTempDirLen >= nFilePathLen || pszFilePath[nTempDirLen-1] != '\\')
        return;

    // shorten the directory until we're down to the part that exists
    do
    {
        while (pszFilePath[i] != '\\')
            i--;
        if (i < nTempDirLen)
            break;
        pszFilePath[i] = 0;
    }
    while (GetFileAttributes(pszFilePath) == 0xFFFFFFFF);

    // build it up again creating each sub-directory along the way
    for (;;)
    {
        pszFilePath[i] = '\\';
        while (pszFilePath[i] != 0)
            i++;
        if (i == nFilePathLen)
            return;
        CreateDirectory(pszFilePath, NULL);
    }
}

//////////////////////////////////////////////////////////////////////
// Random number generator

int Random(int& nSeed) 
{ 
    const int nRandFactor = 0x015A4E35;
    const int nRandMax = 0x7FFF;

    return ((nSeed = nSeed * nRandFactor + 1) >> 16) & nRandMax; 
}

//////////////////////////////////////////////////////////////////////
// Create a temporary directory

BOOL CreateTempDir(LPTSTR pszDir)
{
    if (IsCancelled())
        return LoadOsError(IDS_ERR_EXTRACTING, ERROR_CANCELLED);

    TCHAR szTemp[MAX_PATH];
    int nTries = 0;
    BOOL bDone = FALSE;

    szTemp[0] = 0;
    GetTempPath(MAX_PATH, szTemp);

    // make a seed for the rand numbers from the system time
    SYSTEMTIME st;
    GetSystemTime(&st);
    int nSeed = st.wYear % 50;
    nSeed = 12 * nSeed + st.wMonth;
    nSeed = 31 * nSeed + st.wDay;
    nSeed = 24 * nSeed + st.wHour;
    nSeed = 60 * nSeed + st.wMinute;
    nSeed = 60 * nSeed + st.wSecond;

    while (!bDone)
    {
        GetTempFileName(szTemp, TEXT("SLF"), Random(nSeed), pszDir);
        bDone = CreateDirectory(pszDir, NULL);

        DWORD dwErr = GetLastError();
        if ((!bDone && dwErr != ERROR_FILE_EXISTS && dwErr != ERROR_ALREADY_EXISTS) || ++nTries == 100)
            return LoadOsError(IDS_ERR_CREATING);
    }

    // append a trailing backslash
    int nLen = lstrlen(pszDir);
    lstrcpyn(pszDir + nLen, TEXT("\\"), MAX_PATH - nLen);

    return TRUE;
}

//////////////////////////////////////////////////////////////////////
// Extract the files

BOOL ExtractFiles(LPCTSTR pszDestDir, LPTSTR pszExecFile, HWND hwndFeedback, UINT uMsgFeedback)
{
    if (IsCancelled())
        return LoadOsError(IDS_ERR_EXTRACTING, ERROR_CANCELLED);

    // figure out the length of the executable
    int nLen = ParsePEForLength(GetModuleHandle(NULL));
    if (nLen == 0)
        return LoadError(IDS_ERR_EXTRACTING, IDS_ERR_EXELENGTH);

    if (IsCancelled())
        return LoadOsError(IDS_ERR_EXTRACTING, ERROR_CANCELLED);

    // call the extraction code
    TCHAR szFilename[MAX_PATH];
    GetModuleFileName(NULL, szFilename, MAX_PATH);
    return Extract(szFilename, nLen, pszDestDir, pszExecFile, hwndFeedback, uMsgFeedback);
}

//////////////////////////////////////////////////////////////////////
// Delete the temp directory
// Builds up the paths in pszPath rather than using locally declared
// strings to use a bit less stack space

BOOL DeleteTempDir(LPTSTR pszPath)
{
    BOOL bOk = TRUE;
    int nDirLen = lstrlen(pszPath);
    lstrcpyn(pszPath + nDirLen, TEXT("*.*"), MAX_PATH - nDirLen);

    WIN32_FIND_DATA fd;

    HANDLE hFind = FindFirstFile(pszPath, &fd);
    if (hFind == INVALID_HANDLE_VALUE)
    {
        pszPath[nDirLen] = 0;
        return LoadOsError(IDS_ERR_DELETING);
    }

    do
    {
        if (lstrcmp(fd.cFileName, TEXT(".")) == 0 || lstrcmp(fd.cFileName, TEXT("..")) == 0)
            continue;

        int nNameLen = lstrlen(fd.cFileName);

        if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (nDirLen + nNameLen + 1 >= MAX_PATH)
            {
                if (bOk)
                    bOk = LoadError(IDS_ERR_DELETING, IDS_ERR_PATHLENGTH);
                continue;
            }
            lstrcpy(pszPath + nDirLen, fd.cFileName);
            lstrcat(pszPath, TEXT("\\"));
            bOk = DeleteTempDir(pszPath) && bOk;
        }
        else
        {
            if (nDirLen + nNameLen >= MAX_PATH)
            {
                if (bOk)
                    bOk = LoadError(IDS_ERR_DELETING, IDS_ERR_PATHLENGTH);
                continue;
            }
            lstrcpy(pszPath + nDirLen, fd.cFileName);
            DeleteFile(pszPath);
        }
    }
    while (FindNextFile(hFind, &fd));

    FindClose(hFind);
    if (nDirLen > 0) 
    {
        pszPath[nDirLen] = 0;
        pszPath[nDirLen-1] = 0;

        if (!RemoveDirectory(pszPath) && bOk)
            bOk = LoadOsError(IDS_ERR_DELETING);
        
        pszPath[nDirLen-1] = '\\';
    }

    return bOk;
}

//////////////////////////////////////////////////////////////////////
// Check for any open files
// Builds up the paths in pszPath rather than using locally declared
// strings to use a bit less stack space

BOOL FindOpenFile(LPTSTR pszPath, LPTSTR pszOpenFile)
{
    int nDirLen = lstrlen(pszPath);
    lstrcpyn(pszPath + nDirLen, TEXT("*.*"), MAX_PATH - nDirLen);

    WIN32_FIND_DATA fd;

    HANDLE hFind = FindFirstFile(pszPath, &fd);
    if (hFind == INVALID_HANDLE_VALUE)
    {
        pszPath[nDirLen] = 0;
        return FALSE;
    }

    do
    {
        if (lstrcmp(fd.cFileName, TEXT(".")) == 0 || lstrcmp(fd.cFileName, TEXT("..")) == 0)
            continue;

        if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (nDirLen + lstrlen(fd.cFileName) + 1 >= MAX_PATH)
                continue;
            lstrcpy(pszPath + nDirLen, fd.cFileName);
            lstrcat(pszPath, TEXT("\\"));

            if (FindOpenFile(pszPath, pszOpenFile))
            {
                pszPath[nDirLen] = 0;
                FindClose(hFind);
                return TRUE;
            }
        }
        else
        {
            if (nDirLen + lstrlen(fd.cFileName) >= MAX_PATH)
                continue;
            lstrcpy(pszPath + nDirLen, fd.cFileName);

            HANDLE hFile = CreateFile(pszPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
            if (hFile == INVALID_HANDLE_VALUE && GetLastError() == ERROR_SHARING_VIOLATION)
            {
#ifdef _DEBUG
                OutputDebugString(pszPath);
                OutputDebugString(TEXT(" is in use\n"));
#endif
                lstrcpy(pszOpenFile, pszPath);
                pszPath[nDirLen] = 0;
                FindClose(hFind);
                return TRUE;
            }
            if (hFile != INVALID_HANDLE_VALUE)
                CloseHandle(hFile);
        }
    }
    while (FindNextFile(hFind, &fd));

    pszPath[nDirLen] = 0;
    FindClose(hFind);
    return FALSE;
}

//////////////////////////////////////////////////////////////////////
// Run the program

BOOL RunProgram(LPTSTR pszDir, LPCTSTR pszExecFile, HWND hwndFeedback)
{
    TCHAR szPath[MAX_PATH];

    // use the directory of pszExecFile for it's working directory
    lstrcpy(szPath, pszExecFile);
    LPTSTR p = szPath + lstrlen(szPath);
    while (p > szPath && *p != '\\')
        p--;
    *p = 0;

    int nLen = lstrlen(pszExecFile);
    if ((nLen < 5 || lstrcmpi(pszExecFile + nLen - 5, TEXT(".html")) != 0) &&
        (nLen < 4 || lstrcmpi(pszExecFile + nLen - 4, TEXT(".htm")) != 0))
    {
        // ShellExecuteEx is the normal way to do this
        SHELLEXECUTEINFO sei;
        ZeroMemory(&sei, sizeof(sei));
        sei.cbSize = sizeof(sei);
        sei.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
        sei.hwnd = hwndFeedback;
        sei.lpFile = pszExecFile;
        sei.lpDirectory = szPath;
        sei.nShow = SW_NORMAL;
        ShellExecuteEx(&sei);
        SendMessage(hwndFeedback, WM_CLOSE, 0, 0);
        WaitForSingleObject(sei.hProcess, INFINITE);
        CloseHandle(sei.hProcess);
    }
    else
    {
        // An alternative is to use CreateProcess since ShellExecuteEx
        // doesn't always return an hProcess to wait on
        PROCESS_INFORMATION pi;
        STARTUPINFO  si;
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        if (!CreateProcess(pszExecFile, NULL, NULL, NULL, FALSE, 0, NULL, szPath, &si, &pi))
        {
            TCHAR szApp[MAX_PATH * 2 + 4];
            if (FindExecutable(pszExecFile, szPath, szApp + 1) <= HINSTANCE(HINSTANCE_ERROR))
                return LoadOsError(IDS_ERR_RUNNING, ERROR_NO_ASSOCIATION);
            *szApp = '"';
            lstrcat(szApp, TEXT("\" \""));
            lstrcat(szApp, pszExecFile);
            lstrcat(szApp, TEXT("\""));
            if (!CreateProcess(NULL, szApp, NULL, NULL, FALSE, 0, NULL, szPath, &si, &pi))
                return LoadOsError(IDS_ERR_RUNNING);
        }

        SendMessage(hwndFeedback, WM_CLOSE, 0, 0);
        WaitForSingleObject(pi.hProcess, INFINITE);
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }

#ifdef _DEBUG
    OutputDebugString(TEXT("WaitForSingleObject returns\n"));
#endif

    // check whether any files are in use
    lstrcpy(szPath, pszDir);
    while (FindOpenFile(pszDir, szPath))
    {
        HANDLE hFile;

        do
        {
            Sleep(1000);
            hFile = CreateFile(szPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
        }
        while (hFile == INVALID_HANDLE_VALUE && GetLastError() == ERROR_SHARING_VIOLATION);

        if (hFile != INVALID_HANDLE_VALUE)
            CloseHandle(hFile);
    }

    return TRUE;
}

//////////////////////////////////////////////////////////////////////
// Worker code, extracts the files and runs them

BOOL RunWorker(HWND hwndFeedback, UINT uMsgFeedback)
{
    TCHAR szDestDir[MAX_PATH];
    TCHAR szExecFile[MAX_PATH];
    szExecFile[0] = 0;

    if (!CreateTempDir(szDestDir))
        return FALSE;
    if (!ExtractFiles(szDestDir, szExecFile, hwndFeedback, uMsgFeedback))
        goto handle_error;
    if (!RunProgram(szDestDir, szExecFile, hwndFeedback))
        goto handle_error;

    return DeleteTempDir(szDestDir);

handle_error:
    DeleteTempDir(szDestDir);
    return FALSE;
}

} // namespace
