/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Work out what the overall file length of an executable should be 
// from the PE headers

#include "stdafx.h"
#include "PEMem.h"

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// Workout length of a mapped file using a pointer to it.
// Doesn't parse debug directories. Returns length or zero for error.

int ParsePEForLength(LPCVOID pvFile, DWORD dwSize)
{
    LPBYTE p = (LPBYTE)pvFile;
    LPBYTE pEnd = p + dwSize;

    // the header of the DOS stub
    if (p + sizeof(IMAGE_DOS_HEADER) > pEnd)
        return 0;
    PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)p;
    if (pDosHdr->e_magic != IMAGE_DOS_SIGNATURE || pDosHdr->e_lfanew < 0)
        return 0;
    p += pDosHdr->e_lfanew;

    // the PE signature
    LPDWORD pdwSig = (LPDWORD)p;
    if ((p += sizeof(DWORD)) > pEnd)
        return 0;
    if (*pdwSig != IMAGE_NT_SIGNATURE)
        return 0;

    // the PE header
    PIMAGE_FILE_HEADER pPeHdr = (PIMAGE_FILE_HEADER)p;
    if ((p += sizeof(IMAGE_FILE_HEADER)) > pEnd)
        return 0;

    // the 'optional' PE header
    if ((p += pPeHdr->SizeOfOptionalHeader) > pEnd)
        return 0;

    // the section headers
    DWORD dwLen = 0;
    for (int i = 0; i < pPeHdr->NumberOfSections; i++)
    {
        PIMAGE_SECTION_HEADER pSectHdr = (PIMAGE_SECTION_HEADER)p;
        if ((p += sizeof(IMAGE_SECTION_HEADER)) > pEnd)
            return 0;

        dwLen = max(dwLen, pSectHdr->PointerToRawData + pSectHdr->SizeOfRawData);
    }

    // return the result
    return dwLen;
}

//////////////////////////////////////////////////////////////////////
// Work out length from a loaded module. 
// Doesn't parse debug directories. Returns 0 for error.

int ParsePEForLength(HMODULE hModule)
{
    MEMORY_BASIC_INFORMATION mbi;

    if (VirtualQuery(hModule, &mbi, sizeof(mbi)) < sizeof(mbi) || mbi.BaseAddress != hModule)
        return 0;
    return ParsePEForLength(mbi.BaseAddress, mbi.RegionSize);
}

} // namespace
