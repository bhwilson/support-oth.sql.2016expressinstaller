/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/////////////////////////////////////////////////////////////////////////////
//
// SelfExt Version numbers
//
// The product and file versions are all synchronised using these values
//

#ifdef _DEBUG
#   define DEBUGSTR "D"
#else
#   define DEBUGSTR
#endif

#ifdef EXTFDI
#   define COMPRESSTYPE " FDI"
#else
#   define COMPRESSTYPE " Cab"
#endif

#define VERMAJOR 1
#define VERMINOR 0
#define VERPATCH 0
#define VERBUILD 35

#define VERSTR "1.0.0"
#define VERTYPESTR VERSTR DEBUGSTR COMPRESSTYPE

#define CONTACT "OnTheHouse Support"
#define COMPANYNAME "OnTheHouse" CONTACT
#define PRODUCTNAME "Self-extracting CAB file"
#define COPYRIGHTSTR "1992-2013 Console Australia Pty Ltd"
