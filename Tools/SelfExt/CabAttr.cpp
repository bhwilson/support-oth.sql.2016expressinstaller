/*
 * This file is part of CabAttr, display or change cabinet file attributes
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * CabAttr is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CabAttr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CabAttr; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// View/Set file attributes of files in a cabinet

#include <windows.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstddef>
#include <cstdio>
#include <cctype>

#include "pefile.h"
#include "cab.h"
#include "selfver.h"

// Turn off command line globbing when compiled with MinGW's g++
int _CRT_glob = 0;

using namespace std;

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// A class to represent the program

class CCabAttr
{
public:
    // program return codes
    enum RetVal
    { 
        RET_OK,         // success
        RET_PARTIAL,    // some, but not all, cabs processed successfully
        RET_FAIL,       // failure
        RET_PARAM       // bad command line parameters
    };

private:
    u2 m_wAddMask;      // u2 is unsigned 2-byte value (in cab speak)
    u2 m_wRemoveMask;
    char *m_pszCab;
    char *m_pszTarget;
    bool m_bUpdate;

public:
    // interface
    CCabAttr(int argc, char *argv[]);
    int Run();

private:
    // print error and throw execption
    static void HelpStop(bool license = false);
    static void BadParamStop(char *pszMsg, char *pszArg = "");
    static void OsErrorStop(char *pszMsg1, char *pszMsg2 = "");
    static void ErrorStop(char *pszMsg1, char *pszMsg2 = "");

    // convert attribs & date-times to/from string
    static u2 CharToAttrib(int cLetter);
    static char *AttribToStr(char *psz, u2 attrib);
    static char *DateTimeStr(char *psz, u2 wDate, u2 wTime);

    // processing
    int ProcessCab(char *pszCab);
    void ReadCab(char *pszCab, fstream& cabfile, void *pv, int size);
    bool Match(const char *pszWild, const char *pszFile);
};


//////////////////////////////////////////////////////////////////////
// Handle, with close on destruction

class CFindHandle
{
    HANDLE m_h;
public:
    CFindHandle(HANDLE h) { m_h = h; }
    ~CFindHandle() { if (m_h != INVALID_HANDLE_VALUE) FindClose(m_h); }
    operator HANDLE() { return m_h; }
};

} // namespace


//////////////////////////////////////////////////////////////////////
// main

int main(int argc, char *argv[])
{
    using namespace chiclero;
    cout << "CabAttr " << VERSTR;
    cout << " - Display or change the attributes of files in a cabinet.";
    cout << endl;

    try
    {
        CCabAttr theApp(argc, argv);
        return theApp.Run();
    }
    catch (CCabAttr::RetVal nExitCode)
    {
        return nExitCode;
    }
}


namespace chiclero {

//////////////////////////////////////////////////////////////////////
// constructor - parse the command line

CCabAttr::CCabAttr(int argc, char *argv[])
{
    m_wAddMask = m_wRemoveMask = 0;
    m_pszCab = m_pszTarget = NULL;

    // no parameters - show help
    if (argc <= 1)
        HelpStop();

    for (int i = 1; i < argc; i++)
    {
        // switches
        if (argv[i][0] == '/')
        {
            if (argv[i][1] == '?' && argv[i][2] == 0)
                HelpStop();
            else if (toupper(argv[i][1]) == 'L' && argv[i][2] == 0)
                HelpStop(true);
            else
                BadParamStop("Unknown switch: ", argv[i]);
        }

        // attibutes
        else if (argv[i][0] == '-' || argv[i][0] == '+')
        {
            int ch = CharToAttrib(argv[i][1]);
            if (!ch || argv[i][2])
                BadParamStop("Attribute not recognised: ", argv[i]);
            if ((m_wRemoveMask | m_wAddMask) & ch)
                BadParamStop("Attribute specified more than once: ", argv[i]);
            if (argv[i][0] == '-')
                m_wRemoveMask |= ch;
            else
                m_wAddMask |= ch;
        }

        // cab name
        else if (m_pszCab == NULL)
            m_pszCab = argv[i];

        // file name (of file(s) inside the cab)
        else if (m_pszTarget == NULL)
            m_pszTarget = argv[i];

        else
            BadParamStop("Too many parameters: ", argv[i]);
    }

    // no cab name is an error
    if (m_pszCab == NULL)
        BadParamStop("Cabinet name not given.");

    // no target defaults to all files
    if (m_pszTarget == NULL)
        m_pszTarget = "*.*";
    else
        strupr(m_pszTarget);    // aid case insensitive matching

    // updating or just viewing?
    m_bUpdate = m_wAddMask || m_wRemoveMask;

    cout << "\n";
}


//////////////////////////////////////////////////////////////////////
// print help and exit

void CCabAttr::HelpStop(bool license /*=false*/)
{
    cout << COPYRIGHTSTR ", " CONTACT "\n\n";

    if (license)
    {
        cout <<
        "CabAttr is free software; you can redistribute it and/or modify\n"
        "it under the terms of the GNU General Public License as published by\n"
        "the Free Software Foundation; either version 2 of the License, or\n"
        "(at your option) any later version.\n"
        "\n"
        "CabAttr is distributed in the hope that it will be useful,\n"
        "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
        "GNU General Public License for more details.\n"
        "\n"
        "You should have received a copy of the GNU General Public License\n"
        "along with CabAttr; if not, write to the Free Software\n"
        "Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n";
    }
    else
    {
        cout << "CabAttr [/L] [+R|-R] [+A|-A] [+S|-S] [+H|-H] [+X|-X] Cabinet [Stored File]\n\n";

        cout << " /L   Displays license and warranty information and exits.\n";
        cout << "  +   Sets an attribute.\n";
        cout << "  -   Clears an attribute.\n";
        cout << "  R   Read-only file attribute.\n";
        cout << "  A   Archive file attribute.\n";
        cout << "  S   System file attribute.\n";
        cout << "  H   Hidden file attribute.\n";
        cout << "  X   Execute file after extraction.\n";
        cout << "  U   Filename is UTF encoded (view only).\n\n";

        cout << "The cabinet name and the stored file name can include wildcards.\n";
    }

    throw RET_OK;
}


//////////////////////////////////////////////////////////////////////
// print bad parameter error message and exit

void CCabAttr::BadParamStop(char *pszMsg, char *pszArg /*=""*/)
{
    cerr << pszMsg << pszArg << "\n\n";
    cerr << "Use '/?' for help\n";
    throw RET_PARAM;
}


//////////////////////////////////////////////////////////////////////
// print a error message with perror and exit

void CCabAttr::OsErrorStop(char *pszMsg1, char *pszMsg2 /*=""*/)
{
    cerr << pszMsg1 << pszMsg2 << ": ";
    perror(NULL);
    throw RET_FAIL;
}

//////////////////////////////////////////////////////////////////////
// print error message and exit

void CCabAttr::ErrorStop(char *pszMsg1, char *pszMsg2 /*=""*/)
{
    cerr << pszMsg1 << pszMsg2 << "\n";
    throw RET_FAIL;
}


//////////////////////////////////////////////////////////////////////
// convert from display letters to attribute values

u2 CCabAttr::CharToAttrib(int cLetter)
{
    switch (toupper(cLetter))
    {
        case 'R': return _A_RDONLY;
        case 'H': return _A_HIDDEN;
        case 'S': return _A_SYSTEM;
        case 'A': return _A_ARCH;
        case 'X': return _A_EXEC;
        default : return 0;
    }
}


//////////////////////////////////////////////////////////////////////
// convert attribute values to display letters (7 byte buffer required)

char *CCabAttr::AttribToStr(char *psz, u2 attrib)
{
    strcpy(psz, "------");

    if (attrib & _A_RDONLY)
        psz[0] = 'R';
    if (attrib & _A_HIDDEN)
        psz[1] = 'H';
    if (attrib & _A_SYSTEM)
        psz[2] = 'S';
    if (attrib & _A_ARCH)
        psz[3] = 'A';
    if (attrib & _A_EXEC)
        psz[4] = 'X';
    if (attrib & _A_NAME_IS_UTF)
        psz[5] = 'U';

    return psz;
}


//////////////////////////////////////////////////////////////////////
// Run - iterate over all the cab files

int CCabAttr::Run()
{
    char szPath[MAX_PATH];
    int nDir = 0;
    int nRetVal = RET_FAIL;

    strncpy(szPath, m_pszCab, MAX_PATH);
    szPath[MAX_PATH-1] = 0;

    // if the name doesn't have a dot, then assume '.cab'
    char *p = strrchr(szPath, '\\');
    if (p)
    {
        nDir = p - szPath + 1;
        p = strchr(p, '.');
    }
    else
        p = strchr(szPath, '.');
    if (!p)
        strncat(szPath, ".cab", MAX_PATH-strlen(szPath));

    // wildcards?
    if (strchr(m_pszCab, '?') || strchr(m_pszCab, '*'))
    {
        WIN32_FIND_DATA fd;
        CFindHandle hFind = FindFirstFile(szPath, &fd);

        if (hFind == INVALID_HANDLE_VALUE)
        {
            cerr << "No files found.\n";
            throw RET_FAIL;
        }

        do
        {
            if (m_bUpdate)
                cout << "Updating ";
            else
                cout << "Listing ";
            cout << fd.cFileName << "\n";

            szPath[nDir] = 0;
            strncat(szPath, fd.cFileName, MAX_PATH-nDir);

            try
            {
                ProcessCab(szPath);
                if (nRetVal == RET_FAIL)
                    nRetVal = RET_OK;
            }
            catch (...)
            {
                cout << "\n";
                nRetVal = RET_PARTIAL;
            }
        }
        while (FindNextFile(hFind, &fd));
    }
    else
    {
        if (m_bUpdate)
            cout << "Updating " << szPath << "\n";

        // no wildcards - one file only
        nRetVal = ProcessCab(szPath);
    }

    return nRetVal;
}

    
//////////////////////////////////////////////////////////////////////
// Read from a cab file

void CCabAttr::ReadCab(char *pszCab, fstream& cabfile, void *pv, int size)
{
    if (!cabfile.read(static_cast<char*>(pv), size))
        OsErrorStop("Error reading CAB file ", pszCab);
}


//////////////////////////////////////////////////////////////////////
// process a single cab file

int CCabAttr::ProcessCab(char *pszCab)
{
    // open cab
    fstream cabfile(pszCab, ios::in | ios::binary | (m_bUpdate ? ios::out : 0));
    if (!cabfile)
        OsErrorStop("Error opening CAB file ", pszCab);

    // if this is a WIN32 executable then seek past the PE header
    long lOffset = ParsePEForLength(cabfile);
    if (lOffset == -1)
        lOffset = 0;
    cabfile.seekg(lOffset);

    // read the Cab header and seek to the file list
    CFHEADER hdr;
    ReadCab(pszCab, cabfile, &hdr, sizeof(hdr));
    if (long(hdr.coffFiles) < 0 ||
        hdr.signature[0] != 'M' || hdr.signature[1] != 'S' || 
        hdr.signature[2] != 'C' || hdr.signature[3] != 'F' ||
        !cabfile.seekg(hdr.coffFiles + lOffset))
    {
        ErrorStop(pszCab, " is not a valid CAB file.");
    }

    // iterate over the files
    CFFILE fil;
    int nCount = 0;
    char szAttribs[10];
    char szDateTime[20];
    char szFilename[MAX_PATH];
    char szUFilename[MAX_PATH+1];
    long lOffsetThis;

    for (int i = 0; i < hdr.cFiles; i++)
    {
        if (m_bUpdate && (lOffsetThis = cabfile.tellg()) == -1)
            OsErrorStop("Error reading CAB file ", pszCab);

        ReadCab(pszCab, cabfile, &fil, sizeof(fil));

        // read in the filename
        int j = 0;
        for (;;)
        {
            ReadCab(pszCab, cabfile, szFilename + j, sizeof(char));
            if (szFilename[j] == 0)
                break;
            if (j < MAX_PATH - 1)
                j++;
        }

        // Make an uppercase copy of the filename and if the name part
        // doesn't have a dot, append one. This makes the wildcard
        // matching case insensitive a bit dos like.
        strcpy(szUFilename, szFilename);
        char *p = strrchr(szUFilename, '\\');
        if (!p) p = szUFilename;
        if (strchr(p, '.') == NULL)
            strcat(p, ".");
        strupr(szUFilename);
        
        // if matching target string
        if (!Match(m_pszTarget, szUFilename))
            continue;

        nCount++;

        // displaying or updating?
        if (m_bUpdate)
        {
            // updating
            u2 wNewAttribs = (fil.attribs & ~m_wRemoveMask) | m_wAddMask;

            if (wNewAttribs != fil.attribs)
            {
                int nGetP = cabfile.tellg();
                if (
                    nGetP == -1 ||
                    !cabfile.seekp(lOffsetThis + offsetof(CFFILE, attribs)) ||
                    !cabfile.write((char*)(&wNewAttribs), sizeof(wNewAttribs)) ||
                    !cabfile.seekg(nGetP)
                   )
                {
                    OsErrorStop("Error updating CAB file ", pszCab);
                }
                fil.attribs = wNewAttribs;
            }
        }

        // heading
        if (nCount == 1)
        {
            cout << "     Size Date     Time  Attrs  File Name\n";
            cout << " -------- -------- ----- ------ -----------------------------------------------\n";
        }

        // output a row
        cout << setw(9) << fil.cbFile << " " 
             << DateTimeStr(szDateTime, fil.date, fil.time) << " "
             << AttribToStr(szAttribs, fil.attribs) << " "
             << szFilename << "\n";
    }

    // end
    if (nCount == 0)
        cout << "No files found.\n";
    cout << "\n";

    return RET_OK;
}


//////////////////////////////////////////////////////////////////////
// Decode the time and date and format it as a string
// DD/DD/DD TT:TT (20 byte buffer assmed)

char *CCabAttr::DateTimeStr(char *psz, u2 wDate, u2 wTime)
{
    const int nBufSize = 20;
    FILETIME filetime;
    SYSTEMTIME systime;
    
    if (
        DosDateTimeToFileTime(wDate, wTime, &filetime) &&
        FileTimeToSystemTime(&filetime, &systime) &&
        GetDateFormat(LOCALE_USER_DEFAULT, 0, &systime, NULL, psz, nBufSize)
       )
    {
        int nLen = strlen(psz);
        if (nLen < nBufSize - 2)
        {
            psz[nLen++] = ' ';
            if (GetTimeFormat(
                    LOCALE_USER_DEFAULT,
                    TIME_FORCE24HOURFORMAT | TIME_NOSECONDS, 
                    &systime, NULL,
                    psz + nLen, nBufSize - nLen))
                return psz;
        }
    }

    return strcpy(psz, "              ");

}


//////////////////////////////////////////////////////////////////////
// return true if the wildcard matches the filename

bool CCabAttr::Match(const char *pszWild, const char *pszFile)
{
    while (*pszWild)
    {
        // '*' matches anything
        if (*pszWild == '*')
        {
            pszWild++;

            // move past any more wildcards
            while (*pszWild == '*' || *pszWild == '?')
            {
                if (*pszWild == '?')
                {
                    if (*pszFile == 0)
                        return false;
                    pszFile++;
                }
                pszWild++;
            }

            // if we're now at the end of the wild
            // string then they matched
            if (*pszWild == 0)
                return true;

            // does the character following the
            // wildcards match?
            while ((pszFile = strchr(pszFile, *pszWild)) != NULL)
                if (Match(pszWild, pszFile++))
                    return true;
            return false;
        }

        // ? matches single character
        else if (*pszWild == '?')
        {
            if (*pszFile == 0)
                return false;
            pszWild++;
            pszFile++;
        }

        // anything else must be equal
        else
        {
            if (*pszWild++ != *pszFile++)
                return false;
        }
    }

    return *pszFile == 0;
}

} // namespace
