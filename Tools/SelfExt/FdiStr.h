/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/////////////////////////////////////////////////////////////////////////////
// Strings specific to the FDI Cab version
//
// Uses IDs in the range 0x0020 - 0x002F

#define IDS_ERR_CABINIT                 32
#define IDS_ERR_CHAINED                 33
#define IDS_FDIERROR_NONE               36
#define IDS_FDIERROR_CABINET_NOT_FOUND  37
#define IDS_FDIERROR_NOT_A_CABINET      38
#define IDS_FDIERROR_UNKNOWN_CABINET_VERSION 39
#define IDS_FDIERROR_CORRUPT_CABINET    40
#define IDS_FDIERROR_ALLOC_FAIL         41
#define IDS_FDIERROR_BAD_COMPR_TYPE     42
#define IDS_FDIERROR_MDI_FAIL           43
#define IDS_FDIERROR_TARGET_FILE        44
#define IDS_FDIERROR_USER_ABORT         47
