/*
 * This file is part of CabAttr, display or change cabinet file attributes
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * CabAttr is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CabAttr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CabAttr; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Work out what the overall file length of an executable should be 
// from the PE headers

#include <windows.h>
#include "PEFile.h"

using namespace std;

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// Work out length of an executable from a file handle to it.
// Return 0 for error in executable's format, or -1 for OS error.

int ParsePEForLength(fstream& f)
{
    // read the header of the DOS stub
    IMAGE_DOS_HEADER hdrDOS;
    typedef char* pchar;

    if (!f.read(pchar(&hdrDOS), sizeof(hdrDOS)))
        return -1;        
    if (hdrDOS.e_magic != IMAGE_DOS_SIGNATURE)
        return 0;

    // read the PE header
    DWORD dw;
    IMAGE_FILE_HEADER hdrPE;

    if (
        !f.seekg(hdrDOS.e_lfanew) || 
        !f.read(pchar(&dw), sizeof(dw)) ||
        !f.read(pchar(&hdrPE), sizeof(hdrPE))
        )
        return -1;

    // assume optional header is as least as big as in WINNT.H
    if (dw != IMAGE_NT_SIGNATURE || hdrPE.SizeOfOptionalHeader < sizeof(IMAGE_OPTIONAL_HEADER))
        return 0;

    // read the 'optional' PE header
    IMAGE_OPTIONAL_HEADER hdrOpt;

    if (!f.read(pchar(&hdrOpt), sizeof(hdrOpt)))
        return -1;
    if (hdrOpt.NumberOfRvaAndSizes < IMAGE_NUMBEROF_DIRECTORY_ENTRIES)
        return 0;
    if (hdrPE.SizeOfOptionalHeader > sizeof(hdrOpt))
        f.seekg(hdrPE.SizeOfOptionalHeader - sizeof(hdrOpt), ios::cur);

    // read the section headers
    IMAGE_SECTION_HEADER hdrSect;
    DWORD dwLen = 0;

    DWORD dwDebugVirt = hdrOpt.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress;
    DWORD dwDebugSize = hdrOpt.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size;
    DWORD dwDebugOffset = 0;

    for (int i = 0; i < hdrPE.NumberOfSections; i++)
    {
        if (!f.read(pchar(&hdrSect), sizeof(hdrSect)))
            return -1;

        dwLen = max(dwLen, hdrSect.PointerToRawData + hdrSect.SizeOfRawData);

        // if the debug directory is in this section then convert it's
        // virtual address to a file offset
        DWORD dwSectVirt = hdrSect.VirtualAddress;
        if (dwDebugVirt >= dwSectVirt && dwDebugVirt < dwSectVirt + hdrSect.SizeOfRawData)
            dwDebugOffset = dwDebugVirt - dwSectVirt + hdrSect.PointerToRawData;
    }

    // if a debug directory is present then parse
    if (dwDebugOffset && dwDebugSize)
    {
        if (dwDebugSize % sizeof(IMAGE_DEBUG_DIRECTORY))
            return 0;    // excepting a whole number of dirs

        int nNumDirs = dwDebugSize / sizeof(IMAGE_DEBUG_DIRECTORY);

        if (!f.seekg(dwDebugOffset))
            return -1;

        IMAGE_DEBUG_DIRECTORY dirDbg;

        for (int i = 0; i < nNumDirs; i++)
        {
            if (!f.read(pchar(&dirDbg), sizeof(dirDbg)))
                return -1;

            dwLen = max(dwLen, dirDbg.PointerToRawData + dirDbg.SizeOfData);
        }
    }

    // round up to a multiple of 0x100
    dwLen = (dwLen + 0xFF) & 0xFFFFFF00;

    // return the result
    return dwLen;
}

} // namespace

