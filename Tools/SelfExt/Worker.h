/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Worker code, extracts the files and runs the program

#if !defined(WORKER_H)
#define WORKER_H

namespace chiclero {

// Worker module's entry point
BOOL RunWorker(HWND hwndFeedback, UINT uMsgFeedback);

// Implemented in the decompression modules (ExtCab.cpp or ExtFDI.cpp
// depending on the compression type being used)
BOOL Extract(LPCTSTR pszFilename, int nOffset, LPCTSTR pszDestDir, 
             LPTSTR pszExecFile, HWND hwndFeedback, UINT uMsgFeedback);

// The following are implemented by Worker, for use by the 
// decompression modules...

// Returns TRUE if the user wants to cancel, pumps messages
BOOL IsCancelled();

// Make sure the directory for the given file exists, create if not.
void EnsureFileDirectoryExists(LPTSTR pszFilePath, int nTempDirLen);

} // namespace

#endif // !defined(WORKER_H)
