;
; Jump to startup for Borland C++
;

.386
.model flat
.code

extrn   _WinMainCRTStartup:proc

Start:  jmp     _WinMainCRTStartup
        end     Start
