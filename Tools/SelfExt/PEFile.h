/*
 * This file is part of CabAttr, display or change cabinet file attributes
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * CabAttr is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CabAttr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CabAttr; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Work out what the overall file length of an executable should be 
// from the PE headers

#if !defined(PEFILE_H)
#define PEFILE_H

#include <fstream>

namespace chiclero
{
    // returns -1 for OS error, or 0 for other errors
    int ParsePEForLength(std::fstream& f);
}

#endif // !defined(PEFILE_H)
