//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SelfExt.rc
//
#define IDS_ERR_TITLE                   1
#define IDS_ERR_DEFAULT                 3
#define IDS_ERR_EXTRACTING              4
#define IDS_ERR_RUNNING                 5
#define IDS_ERR_DELETING                6
#define IDS_ERR_CREATING                7
#define IDS_ERR_TEMPFILE                8
#define IDS_ERR_PATHLENGTH              10
#define IDS_ERR_EXELENGTH               11
#define IDD_FEEDBACK                    101
#define IDC_FEEDBACK_PROGRESS           1000
#define IDC_FEEDBACK_ICON               1001
#define IDC_FEEDBACK_MSG                1002
#define IDI_SELFEXT                     3000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
