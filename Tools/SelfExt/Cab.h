/*
 * This file is part of CabAttr, display or change cabinet file attributes
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * CabAttr is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CabAttr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CabAttr; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Cab file format definitions

namespace chiclero {

#pragma pack(push, 1)

typedef unsigned char  u1;
typedef unsigned short u2;
typedef unsigned int   u4;

struct CFHEADER
{
    u1  signature[4];       /* cabinet file signature */
    u4  reserved1;          /* reserved */
    u4  cbCabinet;          /* size of this cabinet file in bytes */
    u4  reserved2;          /* reserved */
    u4  coffFiles;          /* offset of the first CFFILE entry */
    u4  reserved3;          /* reserved */
    u1  versionMinor;       /* cabinet file format version, minor */
    u1  versionMajor;       /* cabinet file format version, major */
    u2  cFolders;           /* number of CFFOLDER entries in this cabinet */
    u2  cFiles;             /* number of CFFILE entries in this cabinet */
    u2  flags;              /* cabinet file option indicators */
    u2  setID;              /* must be the same for all cabinets in a set */
    u2  iCabinet;           /* number of this cabinet file in a set */
    //u2  cbCFHeader;       /* (optional) size of per-cabinet reserved area */
    //u1  cbCFFolder;       /* (optional) size of per-folder reserved area */
    //u1  cbCFData;         /* (optional) size of per-datablock reserved area */
    //u1  abReserve[];      /* (optional) per-cabinet reserved area */
    //u1  szCabinetPrev[];  /* (optional) name of previous cabinet file */
    //u1  szDiskPrev[];     /* (optional) name of previous disk */
    //u1  szCabinetNext[];  /* (optional) name of next cabinet file */
    //u1  szDiskNext[];     /* (optional) name of next disk */
};

struct CFFOLDER
{
    u4  coffCabStart;       /* offset of the first CFDATA block in this folder */
    u2  cCFData;            /* number of CFDATA blocks in this folder */
    u2  typeCompress;       /* compression type indicator */
    //u1  abReserve[];      /* (optional) per-folder reserved area */
};

struct CFFILE
{
    u4  cbFile;             /* uncompressed size of this file in bytes */
    u4  uoffFolderStart;    /* uncompressed offset of this file in the folder */
    u2  iFolder;            /* index into the CFFOLDER area */
    u2  date;               /* date stamp for this file */
    u2  time;               /* time stamp for this file */
    u2  attribs;            /* attribute flags for this file */
    //u1  szName[];         /* name of this file */
};

struct CFDATA
{
    u4  csum;               /* checksum of this CFDATA entry */
    u2  cbData;             /* number of compressed bytes in this block */
    u2  cbUncomp;           /* number of uncompressed bytes in this block */
    //u1  abReserve[];      /* (optional) per-datablock reserved area */
    //u1  ab[cbData];       /* compressed data bytes */
};

#ifndef _A_RDONLY
#define _A_RDONLY      (0x01)   /* file is read-only */
#endif

#ifndef _A_HIDDEN
#define _A_HIDDEN      (0x02)   /* file is hidden */
#endif

#ifndef _A_SYSTEM
#define _A_SYSTEM      (0x04)   /* file is a system file */
#endif

#ifndef _A_ARCH
#define _A_ARCH        (0x20)   /* file modified since last backup */
#endif

#ifndef _A_EXEC
#define _A_EXEC        (0x40)   /* run after extraction */
#endif

#ifndef _A_NAME_IS_UTF
#define _A_NAME_IS_UTF (0x80)   /* szName[] contains UTF */
#endif

#pragma pack(pop)

} // namespace
