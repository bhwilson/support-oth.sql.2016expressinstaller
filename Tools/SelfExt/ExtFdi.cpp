/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Extract the files from a cab using the FDI library. Creates a
// larger executable than ExtCab.cpp but works on all Win32 platforms

#include "stdafx.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <Fdi.h>    // from the MS Cabinet SDK

#include "Resource.h"
#include "ErrorStr.h"
#include "Worker.h"
#include "FdiStr.h"

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// Types and constants

struct file_struct
{
    HANDLE  m_hFile;
    int     m_nOffset;
};

struct fdi_copy
{
    LPCSTR  m_pszDestDir;
    int     m_nCount;
    HWND    m_hwndFeedback;
    UINT    m_uMsgFeedback;
    DWORD   m_dwOsErr;
    LPTSTR  m_pszExecFile;
};

const int cSep = '|';

//////////////////////////////////////////////////////////////////////
// returns a pointer to the file_struct object specified by the given
// handle

file_struct *GetPtr(int hf)
{
    if (hf == 0 || IsBadReadPtr((LPVOID)hf, sizeof(file_struct)))
        return NULL;
    else
        return (file_struct*)hf;
}

//////////////////////////////////////////////////////////////////////
// returns the file handle specified by the given handle

HANDLE GetHandle(int hf)
{
    const file_struct *pfs = GetPtr(hf);
    return pfs ? pfs->m_hFile : INVALID_HANDLE_VALUE;
}

//////////////////////////////////////////////////////////////////////
// file open
// if the file name looks like 'filename|<nnnnnn>' then file_open
// pretends that offset <nnnnnn> is the start of the file

FNOPEN(file_open)
{
    LPCSTR p = pszFile;
    while (*p != 0 && *p != cSep)
        p++;
    CHAR szName[MAX_PATH];
    file_struct *pfs = (file_struct*)HeapAlloc(GetProcessHeap(), 0, sizeof(file_struct));

    pfs->m_nOffset = 0;

    // separate out the offset part of the filename
    if (*p)
    {
        lstrcpyn(szName, pszFile, min(p-pszFile+1, MAX_PATH));
        pszFile = szName;
        pfs->m_nOffset = 0;
        while (*++p >= '0' && *p <= '9')
            pfs->m_nOffset = pfs->m_nOffset * 10 + *p - '0';
    }

    int dwAccess = 0, dwCreate = 0;

    // decode the windows file access from oflag
    switch(oflag & (_O_RDONLY|_O_WRONLY|_O_RDWR)) 
    {
        case _O_RDONLY: 
            dwAccess = GENERIC_READ; break;

        case _O_WRONLY: 
            dwAccess = GENERIC_WRITE; break;

        case _O_RDWR:
            dwAccess = GENERIC_READ | GENERIC_WRITE;
    }

    // decode the windows file creation bits from oflag
    switch (oflag & (_O_CREAT|_O_EXCL|_O_TRUNC))
    {
        case 0:
        case _O_EXCL:
            dwCreate = OPEN_EXISTING; break;

        case _O_CREAT:
            dwCreate = OPEN_ALWAYS; break;

        case _O_CREAT | _O_EXCL:
        case _O_CREAT | _O_TRUNC | _O_EXCL:
            dwCreate = CREATE_NEW; break;

        case _O_TRUNC:
        case _O_TRUNC | _O_EXCL:
            dwCreate = TRUNCATE_EXISTING; break;

        case _O_CREAT | _O_TRUNC:
            dwCreate = CREATE_ALWAYS;
    }

    // open the file
    if ((pfs->m_hFile = CreateFile(pszFile, dwAccess, FILE_SHARE_READ, NULL, dwCreate, 0, NULL)) != INVALID_HANDLE_VALUE)
    {
        if (pfs->m_nOffset == 0 || SetFilePointer(pfs->m_hFile, pfs->m_nOffset, NULL, FILE_BEGIN) == (DWORD)pfs->m_nOffset)
            return (int)pfs;

        CloseHandle(pfs->m_hFile);
    }

    HeapFree(GetProcessHeap(), 0, pfs);
    return -1;
}

//////////////////////////////////////////////////////////////////////
// file close

FNCLOSE(file_close)
{
    if (file_struct *pfs = GetPtr(hf))
    {
        BOOL bOk = CloseHandle(pfs->m_hFile);
        HeapFree(GetProcessHeap(), 0, pfs);
        return bOk ? 0 : -1;
    }

    return -1;
}

//////////////////////////////////////////////////////////////////////
// Seek

FNSEEK(file_seek)   
{
    const file_struct *pfs = GetPtr(hf);
    if (pfs == NULL)
        return -1;

    // decode the seek type
    switch (seektype)
    {
        case SEEK_SET: seektype = FILE_BEGIN; break;
        case SEEK_CUR: seektype = FILE_CURRENT; break;
        case SEEK_END: seektype = FILE_END; break;
    }

    long l = SetFilePointer(pfs->m_hFile, seektype == FILE_BEGIN ? dist + pfs->m_nOffset : dist, NULL, seektype);
    return l == -1 ? -1 : l - pfs->m_nOffset;
}

//////////////////////////////////////////////////////////////////////
// the smaller i/o and memory functions for the FDI library

FNALLOC(mem_alloc)
{ 
    return HeapAlloc(GetProcessHeap(), 0, cb); 
}

FNFREE(mem_free)    
{ 
    HeapFree(GetProcessHeap(), 0, pv); 
}

FNREAD(file_read)
{ 
    DWORD rslt;
    ReadFile(GetHandle(hf), pv, cb, &rslt, NULL); 
    return rslt; 
}

FNWRITE(file_write) 
{ 
    DWORD rslt; 
    WriteFile(GetHandle(hf), pv, cb, &rslt, NULL); 
    return rslt; 
}

//////////////////////////////////////////////////////////////////////
// Notification function

FNFDINOTIFY(notify)
{
    switch (fdint)
    {
        // these shouldn't happen since CheckValid should already have
        // rejected chained cabs
        case fdintPARTIAL_FILE:
        case fdintNEXT_CABINET:
            return -1;

        // open a destination file
        case fdintCOPY_FILE:
        {
            if (IsCancelled())  // defined in worker module
                return -1;

            CHAR szDest[MAX_PATH];
            fdi_copy *pcs = (fdi_copy*)pfdin->pv;

            lstrcpyn(szDest, pcs->m_pszDestDir, MAX_PATH);
            int nLen = lstrlen(szDest);
            lstrcpyn(szDest + nLen, pfdin->psz1, MAX_PATH - nLen);

            if (*pcs->m_pszExecFile == 0 || (pfdin->attribs & _A_EXEC))
                lstrcpy(pcs->m_pszExecFile, szDest);

            EnsureFileDirectoryExists(szDest, lstrlen(pcs->m_pszDestDir));

            // set the position of the feedback display with WPARAM
            PostMessage(pcs->m_hwndFeedback, pcs->m_uMsgFeedback, ++pcs->m_nCount, 0);

            int hf = file_open(szDest, _O_BINARY|_O_CREAT|_O_WRONLY, _S_IREAD|_S_IWRITE);
            if (hf == -1)
                pcs->m_dwOsErr = GetLastError();

            return hf;
        }

        // close a destination file, setting time/date and attribs
        case fdintCLOSE_FILE_INFO:
        {
            FILETIME datetime, utc_filetime;
            if (DosDateTimeToFileTime(pfdin->date, pfdin->time, &datetime)
                    && LocalFileTimeToFileTime(&datetime, &utc_filetime))
                SetFileTime(GetHandle(pfdin->hf), &utc_filetime, NULL, &utc_filetime);

            file_close(pfdin->hf);

            CHAR szDest[MAX_PATH];
            fdi_copy *pcs = (fdi_copy*)pfdin->pv;
            lstrcpyn(szDest, pcs->m_pszDestDir, MAX_PATH);
            int nLen = lstrlen(szDest);
            lstrcpyn(szDest + nLen, pfdin->psz1, MAX_PATH - nLen);
            SetFileAttributes(szDest, pfdin->attribs & (FILE_ATTRIBUTE_READONLY | 
                FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_ARCHIVE));

            return TRUE;
        }
    }

    return 0;
}

//////////////////////////////////////////////////////////////////////
// Check a cabinet is there. On success returns the number of files
// in the cabinet, or 0 for failure.

int CheckValid(LPCSTR pszFilename, int nOffset, HFDI hfdi)
{
    if (IsCancelled())
        return LoadOsError(IDS_ERR_EXTRACTING, ERROR_CANCELLED);

    int hf = file_open((LPSTR)pszFilename, _O_BINARY|_O_RDONLY, 0);
    if (hf == -1) 
        return LoadOsError(IDS_ERR_EXTRACTING);

    FDICABINETINFO fdici;

    if (file_seek(hf, nOffset, SEEK_SET) != nOffset)
    {
        LoadOsError(IDS_ERR_EXTRACTING);
        goto handle_error;
    }

    if (!FDIIsCabinet(hfdi, hf, &fdici))
    {
        LoadError(IDS_ERR_EXTRACTING, IDS_FDIERROR_NOT_A_CABINET);
        goto handle_error;
    }

    // can't handle chained cabinets
    if (fdici.hasprev || fdici.hasprev)
    {
        LoadError(IDS_ERR_EXTRACTING, IDS_ERR_CHAINED);
        goto handle_error;
    }

    file_close(hf);
    return fdici.cFiles;

handle_error:
    file_close(hf);
    return 0;
}

//////////////////////////////////////////////////////////////////////
// Extract the files

BOOL ExtractCabFiles(LPCSTR pszFilename, int nOffset, HFDI hfdi, 
                     HWND hwndFeedback, UINT uMsgFeedback,
                     LPCSTR pszDestDir, PERF perf, LPTSTR pszExecFile)
{
    if (IsCancelled())
        return LoadOsError(IDS_ERR_EXTRACTING, ERROR_CANCELLED);

    CHAR szFile[MAX_PATH+11];
    wsprintf(szFile, "%s|%d", pszFilename, nOffset);

    fdi_copy cs;
    cs.m_pszDestDir = pszDestDir;
    cs.m_nCount = 0;
    cs.m_hwndFeedback = hwndFeedback;
    cs.m_uMsgFeedback = uMsgFeedback;
    cs.m_dwOsErr = ERROR_SUCCESS;
    cs.m_pszExecFile = pszExecFile;

    if (!FDICopy(hfdi, szFile, "", 0, notify, NULL, &cs))
    {
        // file_open in the Notificiation function returns -1 on error
        // which looks like a user abort
        if (perf->erfOper == FDIERROR_USER_ABORT && !IsCancelled())
            return LoadOsError(IDS_ERR_EXTRACTING, cs.m_dwOsErr);
        else
            return LoadError(IDS_ERR_EXTRACTING, IDS_FDIERROR_NONE + perf->erfOper);
    }

    return TRUE;
}

//////////////////////////////////////////////////////////////////////
// extract the files from a cab

BOOL Extract(LPCTSTR pszFilename, int nOffset, LPCTSTR pszDestDir,
             LPTSTR pszExecFile, HWND hwndFeedback, UINT uMsgFeedback)
{
    HFDI hfdi;
    ERF  erf;

    // fiddle to allow for the debug directories in the excutable
#if defined(_DEBUG) && defined(_MSC_VER)
    nOffset += 0xA00;
#endif

    // initialise
    hfdi = FDICreate(mem_alloc, mem_free, file_open, file_read,
        file_write, file_close, file_seek, cpu80386, &erf);
    if (hfdi == NULL)
        return LoadError(IDS_ERR_CABINIT, IDS_FDIERROR_NONE + erf.erfOper);

    // check the file is a valid cabinet
    int nNumFiles = CheckValid(pszFilename, nOffset, hfdi);
    if (nNumFiles == 0)
        goto handle_error;
    // set the range of the feedback progress control with LPARAM
    PostMessage(hwndFeedback, uMsgFeedback, 0, nNumFiles);

    if (!ExtractCabFiles(pszFilename, nOffset, hfdi, hwndFeedback, uMsgFeedback, pszDestDir, &erf, pszExecFile))
        goto handle_error;

    FDIDestroy(hfdi);
    return TRUE;

handle_error:
    FDIDestroy(hfdi);
    return FALSE;
}

} // namespace
