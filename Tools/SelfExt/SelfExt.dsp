# Microsoft Developer Studio Project File - Name="SelfExt" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=SelfExt - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SelfExt.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SelfExt.mak" CFG="SelfExt - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SelfExt - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "SelfExt - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "SelfExt - Win32 FDI Release" (based on "Win32 (x86) Application")
!MESSAGE "SelfExt - Win32 FDI Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SelfExt - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Zd /O1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /FR /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 fdi.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib setupapi.lib /nologo /subsystem:windows /map /machine:I386 /nodefaultlib /debugtype:map
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /FR /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib setupapi.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib /pdbtype:sept
# Begin Special Build Tool
OutDir=.\Debug
SOURCE=$(InputPath)
PostBuild_Cmds=copy /b $(OutDir)\selfext.exe+test.cab $(OutDir)\test.exe
# End Special Build Tool

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "FRelease"
# PROP BASE Intermediate_Dir "FRelease"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "FRelease"
# PROP Intermediate_Dir "FRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Zd /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /Zd /O1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /FR /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d "EXTFDI"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib fdi.lib /nologo /subsystem:windows /map /machine:I386 /nodefaultlib /debugtype:map
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib fdi.lib /nologo /subsystem:windows /map /machine:I386 /nodefaultlib /debugtype:map
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "FDebug"
# PROP BASE Intermediate_Dir "FDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "FDebug"
# PROP Intermediate_Dir "FDebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /FR /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG" /d "EXTFDI"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib fdi.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib fdi.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib /pdbtype:sept
# Begin Special Build Tool
OutDir=.\FDebug
SOURCE=$(InputPath)
PostBuild_Cmds=copy /b $(OutDir)\selfext.exe+test.cab $(OutDir)\test.exe
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "SelfExt - Win32 Release"
# Name "SelfExt - Win32 Debug"
# Name "SelfExt - Win32 FDI Release"
# Name "SelfExt - Win32 FDI Debug"
# Begin Group "FDI Specific"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ExtFdi.cpp

!IF  "$(CFG)" == "SelfExt - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\FdiStr.h

!IF  "$(CFG)" == "SelfExt - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\FdiStr.rc2

!IF  "$(CFG)" == "SelfExt - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

!ENDIF 

# End Source File
# End Group
# Begin Group "Cab Specific"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CabStr.h

!IF  "$(CFG)" == "SelfExt - Win32 Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\CabStr.rc2

!IF  "$(CFG)" == "SelfExt - Win32 Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ExtCab.cpp

!IF  "$(CFG)" == "SelfExt - Win32 Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\Copying
# End Source File
# Begin Source File

SOURCE=.\ErrorStr.cpp
# End Source File
# Begin Source File

SOURCE=.\ErrorStr.h
# End Source File
# Begin Source File

SOURCE=.\PEMem.cpp
# End Source File
# Begin Source File

SOURCE=.\PEMem.h
# End Source File
# Begin Source File

SOURCE=.\Readme
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\SelfExt.cpp
# End Source File
# Begin Source File

SOURCE=.\SelfExt.ico
# End Source File
# Begin Source File

SOURCE=.\SelfExt.rc

!IF  "$(CFG)" == "SelfExt - Win32 Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 Debug"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Release"

!ELSEIF  "$(CFG)" == "SelfExt - Win32 FDI Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SelfExt.rc2
# End Source File
# Begin Source File

SOURCE=.\SelfVer.h
# End Source File
# Begin Source File

SOURCE=.\Stdafx.h
# End Source File
# Begin Source File

SOURCE=.\Worker.cpp
# End Source File
# Begin Source File

SOURCE=.\Worker.h
# End Source File
# End Target
# End Project
