/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Extract the files from a cab using the Windows API. Works for
// Windows 95 and later and NT 4.0 and later.
//
// Needs more temporary disk space than the FDI version since
// the API extraction function can't extract straight from the 
// executable, the cab data must be copied out to a file first.

#include "stdafx.h"
#include "Resource.h"
#include "ErrorStr.h"
#include "CabStr.h"
#include "Worker.h"

namespace chiclero {

// the execute attribute defined in the FDI documentation
#ifndef _A_EXEC
#define _A_EXEC 0x40
#endif

//////////////////////////////////////////////////////////////////////
// Types and constants

struct cab_copy
{
    LPCTSTR m_pszDestDir;
    int     m_nCount;
    HWND    m_hwndFeedback;
    UINT    m_uMsgFeedback;
    LPTSTR  m_pszExecFile;
};

//////////////////////////////////////////////////////////////////////
// Notification function

UINT CALLBACK CabMsgHandler(PVOID Context, UINT Notification, UINT Param1, UINT /*Param2*/)
{
    switch (Notification)
    {
        case SPFILENOTIFY_FILEEXTRACTED:
        {
            if (IsCancelled())
                return ERROR_CANCELLED;
            else
                return PFILEPATHS(Param1)->Win32Error;
        }

        case SPFILENOTIFY_FILEINCABINET:
        {
            cab_copy *pcs = (cab_copy*)Context;
            PFILE_IN_CABINET_INFO pfi = PFILE_IN_CABINET_INFO(Param1);

            lstrcpy(pfi->FullTargetName, pcs->m_pszDestDir);
            int nLen = lstrlen(pcs->m_pszDestDir);
            lstrcpyn(pfi->FullTargetName + nLen, pfi->NameInCabinet, MAX_PATH - nLen);

            if (*pcs->m_pszExecFile == 0 || (pfi->DosAttribs & _A_EXEC))
                lstrcpy(pcs->m_pszExecFile, pfi->FullTargetName);

            EnsureFileDirectoryExists(pfi->FullTargetName, lstrlen(pcs->m_pszDestDir));

            // set the position of the feedback display with WPARAM
            PostMessage(pcs->m_hwndFeedback, pcs->m_uMsgFeedback, ++pcs->m_nCount, 0);
            return FILEOP_DOIT;
        }

        case SPFILENOTIFY_NEEDNEWCABINET:
            return ERROR_FILE_NOT_FOUND;    // not supported
    }

    return 0;
}

//////////////////////////////////////////////////////////////////////
// Notification function, this one is used to count the number of 
// files in the cabinet

UINT CALLBACK CountMsgHandler(PVOID Context, UINT Notification, UINT Param1, UINT /*Param2*/)
{
    switch (Notification)
    {
        case SPFILENOTIFY_FILEEXTRACTED:
        {
            if (IsCancelled())
                return ERROR_CANCELLED;
            else
                return PFILEPATHS(Param1)->Win32Error;
        }

        case SPFILENOTIFY_FILEINCABINET:
        {
            (*(int*)Context)++;
            return FILEOP_SKIP;
        }

        case SPFILENOTIFY_NEEDNEWCABINET:
            return ERROR_FILE_NOT_FOUND;    // not supported
    }

    return 0;
}

//////////////////////////////////////////////////////////////////////
// Copy the data from the executable to a temporary file

BOOL CopyData(LPCTSTR pszExeName, DWORD dwOffset, LPTSTR szArcName, HWND hwndFeedback, UINT uMsgFeedback)
{
    HANDLE hHeap = GetProcessHeap();
    HANDLE hFileIn, hFileOut = INVALID_HANDLE_VALUE;
    LPBYTE pBuf = NULL;
    BOOL bOk = FALSE;
    int nBufSize;
    int nBlocksCopied = 0;

    if ((hFileIn = CreateFile(pszExeName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL)) == INVALID_HANDLE_VALUE)
        goto handle_error;
    if ((hFileOut = CreateFile(szArcName, GENERIC_WRITE, FILE_SHARE_READ, NULL, TRUNCATE_EXISTING, 0, NULL)) == INVALID_HANDLE_VALUE)
        goto handle_error;
    if (SetFilePointer(hFileIn, dwOffset, 0, FILE_BEGIN) != dwOffset)
        goto handle_error;

    if ((pBuf = (LPBYTE)HeapAlloc(hHeap, 0, 65536)) == NULL)
        goto handle_error;
    if ((nBufSize = HeapSize(hHeap, 0, pBuf)) <= 0)
        goto handle_error;

    // set the range of the feedback display with LPARAM
    PostMessage(hwndFeedback, uMsgFeedback, 0, (GetFileSize(hFileIn, NULL) - dwOffset) / nBufSize);
    
    DWORD cbRead, cbWritten;

    for (;;)
    {
        if (!ReadFile(hFileIn, pBuf, nBufSize, &cbRead, NULL))
            goto handle_error;
        if (cbRead == 0)
            break;
        if (!WriteFile(hFileOut, pBuf, cbRead, &cbWritten, NULL))
            goto handle_error;

        PostMessage(hwndFeedback, uMsgFeedback, ++nBlocksCopied, 0);

        if (IsCancelled())
        {
            LoadOsError(IDS_ERR_TEMPFILE, ERROR_CANCELLED);
            goto clean_up;
        }
    }

    bOk = TRUE;
    goto clean_up;

handle_error:
    LoadOsError(IDS_ERR_TEMPFILE);
clean_up:
    CloseHandle(hFileIn);
    CloseHandle(hFileOut);
    HeapFree(hHeap, 0, pBuf);
    return bOk;
}

//////////////////////////////////////////////////////////////////////
// extract the files from a cab

BOOL Extract(LPCTSTR pszFilename, int nOffset, LPCTSTR pszDestDir,
             LPTSTR pszExecFile, HWND hwndFeedback, UINT uMsgFeedback)
{
    TCHAR szCabName[MAX_PATH];
    TCHAR szMsg[80];
    int nCount = 0;

    // fiddle to allow for the debug directories in the excutable
#if defined(_DEBUG) && defined(_MSC_VER)
    nOffset += 0x200;
#endif

    if (LoadString(GetModuleHandle(NULL), IDS_MSG_COPY_TEMP, szMsg, sizeof(szMsg)))
        SetDlgItemText(hwndFeedback, IDC_FEEDBACK_MSG, szMsg);

    if (!GetTempPath(MAX_PATH, szCabName) || !GetTempFileName(szCabName, TEXT("CAB"), 0, szCabName))
        return LoadOsError(IDS_ERR_TEMPFILE);

    if (!CopyData(pszFilename, nOffset, szCabName, hwndFeedback, uMsgFeedback))
        goto handle_error;

    if (LoadString(GetModuleHandle(NULL), IDS_MSG_EXTRACT, szMsg, sizeof(szMsg)))
        SetDlgItemText(hwndFeedback, IDC_FEEDBACK_MSG, szMsg);

    // count the files
    if (!SetupIterateCabinet(szCabName, 0, CountMsgHandler, &nCount))
        goto handle_error;

    // set the range of the feedback display with LPARAM
    PostMessage(hwndFeedback, uMsgFeedback, 0, nCount);

    cab_copy cs;
    cs.m_pszDestDir = pszDestDir;
    cs.m_nCount = 0;
    cs.m_hwndFeedback = hwndFeedback;
    cs.m_uMsgFeedback = uMsgFeedback;
    cs.m_pszExecFile = pszExecFile;

    // extract the files
    if (!SetupIterateCabinet(szCabName, 0, CabMsgHandler, &cs))
        goto handle_error;
 
    DeleteFile(szCabName);
    return TRUE;

handle_error:
    LoadOsError(IDS_ERR_EXTRACTING);
    DeleteFile(szCabName);
    return FALSE;
}

} // namespace
