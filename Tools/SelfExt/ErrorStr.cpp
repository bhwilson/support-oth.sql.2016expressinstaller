/*
 * This file is part of SelfExt self extractor
 *
 * Copyright 2002 M.J.Wetherell, http://selfext.sourceforge.net
 *
 * SelfExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SelfExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SelfExt; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//////////////////////////////////////////////////////////////////////
// Load error stings

#include "stdafx.h"
#include "ErrorStr.h"

namespace chiclero {

//////////////////////////////////////////////////////////////////////
// The error messages are supposed to come from the resource
// file, but there is also a hardcoded default string in case 
// even LoadString isn't working

static LPCTSTR szErrDefault = TEXT("An error occurred");
static TCHAR  szErr[ERROR_SIZE];
static BOOL   bErrSet = FALSE;

//////////////////////////////////////////////////////////////////////
// Returns the error message

LPCTSTR GetErrorStr()
{
    bErrSet = FALSE;
    return szErr;
}

//////////////////////////////////////////////////////////////////////
// Error of the format: <string resource 1>: \n<string resource 2>
//
// If either part fails to load the other is returned alone, and if
// neither loads the string is unchanged, so should be preformatted
// with a catch-all message. Always returns FALSE.

BOOL LoadError(UINT nID1, UINT nID2 /*=0*/)
{
    // only the 1st error is used
    if (bErrSet)
        return FALSE;
    bErrSet = TRUE;

    HMODULE hMod = GetModuleHandle(NULL);
    lstrcpy(szErr, szErrDefault);
    int nLen = nID1 ? LoadString(hMod, nID1, szErr, ERROR_SIZE) : 0;

    if (nID2 == 0)
        return FALSE;

    LPTSTR p = nLen ? szErr + nLen + 2 : szErr;

    if (LoadString(hMod, nID2, p, ERROR_SIZE - (p - szErr)) && nLen)
    {
        *--p = '\n';
        *--p = ':';
    }

    return FALSE;
}

//////////////////////////////////////////////////////////////////////
// Error of the format: <string resource>: \n<os error>
// The notes on LoadError apply here too.

BOOL LoadOsError(UINT nID, DWORD dwOsErr /*=GetLastError()*/)
{
    // only the 1st error is used
    if (bErrSet)
        return FALSE;
    bErrSet = TRUE;

    lstrcpy(szErr, szErrDefault);
    int nLen = nID ? LoadString(GetModuleHandle(NULL), nID, szErr, ERROR_SIZE) : 0;

    if (dwOsErr == ERROR_SUCCESS)
        return FALSE;

    LPTSTR p = nLen ? szErr + nLen + 2 : szErr;

    if (FormatMessage(
            FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK,
            NULL, dwOsErr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            p, ERROR_SIZE - (p - szErr), NULL))
    {
        if (nLen)
        {
            *--p = '\n';
            *--p = ':';
        }
    }

    return FALSE;
}

} // namespace
