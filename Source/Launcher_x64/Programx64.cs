﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Threading;
using System.Reflection;

namespace SQL2016Express_Launcher_x64
{
    internal static class Programx64
    {
        private static void Main()
        {

            // Check if OS is compatible.

            int OSMajor = Environment.OSVersion.Version.Major;
            int OSMinor = Environment.OSVersion.Version.Minor;
            int OSBuild = Environment.OSVersion.Version.Build;
            String ServicePack = Environment.OSVersion.ServicePack;

            // This needs to be toggled for 32-bit and 64-bit binary.
            
            if (IntPtr.Size == 4)
            {
                Console.WriteLine("Detected incompatible version of Windows.");
                MessageBox.Show("A 32-bit operating system is not supported by SQL Server 2016." + Environment.NewLine + "Please refer to the minimum System Requirements for SQL Server 2016", "Prerequisite Check Failure");
                goto Cleanup;
            }

            if (OSMajor <= 5) // Pre-Vista OS
            {
                Console.WriteLine("Detected incompatible OS.");
                MessageBox.Show("This operating system is not supported by SQL Server 2016." + Environment.NewLine + "Please refer to the minimum System Requirements for SQL Server 2016", "Prerequisite Check Failure");
                goto Cleanup;
            }
            if (OSMajor == 6 && OSMinor == 0) // Windows Vista & Windows Server 2008
            {
                
                if (OSBuild == 6002) // Windows Server 2008
                {
                    if (!ServicePack.EndsWith("2")) // Windows Server 2008 requires SP2
                    {
                        Console.WriteLine("Detected incompatible OS.");
                        MessageBox.Show("This operating system is not supported by SQL Server 2016." + Environment.NewLine + "Windows Server 2008 requires Service Pack 2.", "Prerequisite Check Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        goto Cleanup;
                    }
                }
                else // Windows Vista
                {
                    Console.WriteLine("Detected incompatible OS.");
                    MessageBox.Show("This operating system is not supported by SQL Server 2016." + Environment.NewLine + "Please refer to the minimum System Requirements for SQL Server 2016", "Prerequisite Check Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    goto Cleanup;
                }
            }
            if (OSMajor == 6 && OSMinor == 0) // Windows 7 & Windows Server 2008 R2
            {
                Console.WriteLine("Detected incompatible OS.");
                MessageBox.Show("This operating system is not supported by SQL Server 2016." + Environment.NewLine + "Please refer to the minimum System Requirements for SQL Server 2016", "Prerequisite Check Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                goto Cleanup;
            
            }

            // OS check complete.

            Console.WriteLine("Preparing to Install SQL Server 2016 Express and a named instance for CONSOLE." + Environment.NewLine);
            Console.WriteLine("Checking Pre-requisites:");

            // Pre-requisite #1: Check if CONSOLE instance already exists.

            ServiceController SC = new ServiceController("MSSQL$CONSOLE");
            bool ServiceIsInstalled = false;
            try
            {
                string ServiceName = SC.DisplayName;
                ServiceIsInstalled = true;
            }
            catch (InvalidOperationException) // PASS - CONSOLE instance does not exist.
            {
                Console.WriteLine("PASS: CONSOLE instance does not exist.");
            }
            finally
            {
                SC.Close();
            }
            if (ServiceIsInstalled == true) // FAIL - CONSOLE instance already exists.
            {
                Console.WriteLine("FAIL: Detected CONSOLE Instance already installed.");
                MessageBox.Show("There is already an instance of CONSOLE installed on this machine." + Environment.NewLine + Environment.NewLine + "This installer is only for new installations of SQL Server." + Environment.NewLine + Environment.NewLine + "Either uninstall all previous instances of SQL Server and run this again, or configure and install SQL Server manually as per our guide.", "Prerequisite Check Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                goto Cleanup;
            }

            // Pre-requisite #1 check complete.
            // Pre-requisite #2: Check if SQL 2016 install already exists.

            if (Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Microsoft SQL Server 13") != null) // Probably a better way of doing this, but it works for now.
            {
                Console.WriteLine("FAIL: Detected previous SQL 2016 installation.");
                MessageBox.Show("There is already an instance of SQL 2016 installed on this machine." + Environment.NewLine + Environment.NewLine + "This installer is only for new installations of SQL Server." + Environment.NewLine + Environment.NewLine + "Either uninstall all previous instances of SQL Server and run this again, or configure and install SQL Server manually as per our guide.", "Pre-requisite Check Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                goto Cleanup;
            }


            // Pre-requisite #2 check complete.
            // Pre-requisite #3: Check if .NET 4.0 installed.

            if (!Programx64.IsDotNet4Installed()) // Install .NET 4.0 if it is not present.
            {
                Console.WriteLine("INFO: .NET 4.0 Requires installing. Launching .NET installer from resources...");
                try
                {
                    Process Net4Install;
                    Net4Install = Process.Start(new ProcessStartInfo()
                    {
                        FileName = Environment.CurrentDirectory + "\\" + "NDP461-KB3102436-x86-x64-AllOS-ENU.exe",
                        Arguments = "/passive"
                        // Example: C:\Users\IEUser\AppData\Local\Temp\SLF5384.tmp\dotNetFx40_Client_x86_x64.exe /passive
                    });
                    Net4Install.WaitForExit();
                    int ECode = Net4Install.ExitCode;

                    if (ECode == 0)
                    {
                        Console.WriteLine("PASS: Installation of .NET 4.0 Completed successfully.");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    }
                    else if (ECode == 1)
                    {
                        Console.WriteLine("FAIL: Installation of .NET 4.0 has been terminated forcefully.");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                        MessageBox.Show("The .NET 4.0 Installer process has been terminated.", "Prerequisite Installation");
                        goto Cleanup;
                    }
                    else if (ECode == 1641)
                    {
                        Console.WriteLine("PASS: Installation of .NET 4.0 Completed successfully.");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    }
                    else if (ECode == 3010)
                    {
                        Console.WriteLine("PASS: Installation of .NET 4.0 Completed successfully.");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    }
                    else if (ECode == 1602)
                    {
                        Console.WriteLine("FAIL: Installation of .NET 4.0 has been cancelled.");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                        MessageBox.Show("The Installation of .NET 4.0 has been cancelled.", "Prerequisite Installation");
                        goto Cleanup;
                    }
                    else if (ECode == 1603)
                    {
                        Console.WriteLine("FAIL: A fatal error has occured during the Installation of .NET 4.0");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                        MessageBox.Show("The Installation of .NET 4.0 has failed." + Environment.NewLine + "Exit Code: " + ECode.ToString() + Environment.NewLine + Environment.NewLine + "A fatal error has occured during the Installation of .NET 4.0", "Prerequisite Installation");
                        goto Cleanup;
                    }
                    else if (ECode == 5100)
                    {
                        Console.WriteLine("FAIL: This machine does not meet .NET 4.0 system requirements.");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                        MessageBox.Show("The Installation of .NET 4.0 has failed." + Environment.NewLine + "Exit Code: " + ECode.ToString() + Environment.NewLine + Environment.NewLine + "This machine does not meet minumum system requirements for .NET 4.0", "Prerequisite Installation");
                        goto Cleanup;
                    }
                    else if (ECode == 5101)
                    {
                        Console.WriteLine("FAIL: An internal state failure has occured during the Installation of .NET 4.0");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                        MessageBox.Show("The Installation of .NET 4.0 has failed." + Environment.NewLine + "Exit Code: " + ECode.ToString() + Environment.NewLine + Environment.NewLine + "An internal state failure has occured during the Installation of .NET 4.0", "Prerequisite Installation");
                        goto Cleanup;
                    }
                    else
                    {
                        Console.WriteLine("FAIL: An unknown error has occured during the Installation of .NET 4.0");
                        Console.WriteLine("EXIT CODE: " + ECode.ToString());
                        MessageBox.Show("The Installation of .NET 4.0 has failed." + Environment.NewLine + "Exit Code: " + ECode.ToString(), "Prerequisite Installation");
                        goto Cleanup;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(Environment.NewLine + "FAIL: The following error has occured while trying to launch the installaiton of .NET 4.0");
                    Console.WriteLine("ERROR: " + "'" + e.Message + "'");
                }
            }


            // Pre-requisite #4 check complete.

            Console.WriteLine("PASS: Prerequisites installed successfully.");
            Console.WriteLine(Environment.NewLine + "Launching installation of SQL Server 2016 Express:");

            // Detect Server or Standard 0S before SQL Server Install.

            String INI = "Default.ini";  // ConfigurationFile

            // Revised INI contents, does not require seperate INI file for different OSTypes anymore.

            /* 
            RegistryKey OSReg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion");
            string OSType = (string)OSReg.GetValue("ProductName");
            if (OSType.Contains("Server")) // Server OS Detected
            {
                Console.WriteLine("INFO: Detected Server OS. Using 'Server.ini' configuration file.");
                INI = "Server.ini";
            }
            else
            {
                Console.WriteLine("INFO: Detected Standard OS. Using 'Standard.ini' configuration file.");
            }
            */

            try // Launch SQL Server installation.
            {
                Process SQLExtract; // Extract contents from SQLEXPR_x64_ENU.exe
                SQLExtract = Process.Start(new ProcessStartInfo()
                {
                    FileName = Environment.CurrentDirectory + "\\" + "SQLEXPR_x64_ENU.exe",
                    Arguments = "/u /x:\"" + Environment.CurrentDirectory + "\\ExtractedMedia\""
                    // Example: C:\Users\IEUser\AppData\Local\Temp\SLF5384.tmp\SQLEXPR_x64_ENU.exe /u /x:"C:\Users\IEUser\AppData\Local\Temp\SLF5384.tmp\ExtractedMedia"
                });
                SQLExtract.WaitForExit();

                Process SQLInstall; // Run Setup.exe from extracted contents with required arguments.
                SQLInstall = Process.Start(new ProcessStartInfo()
                {
                    FileName = Environment.CurrentDirectory + "\\" + "ExtractedMedia" + "\\" + "Setup.exe",
                    Arguments = "/HIDECONSOLE /CONFIGURATIONFILE=\"" + Environment.CurrentDirectory + "\\" + INI + "\""
                    // Example: C:\Users\IEUser\AppData\Local\Temp\SLF5384.tmp\ExtractedMedia\Setup.exe /HIDECONSOLE /CONFIGURATIONFILE="C:\Users\IEUser\AppData\Local\Temp\SLF5384.tmp\Default.ini"
                });
                SQLInstall.WaitForExit();
                int ECode = SQLInstall.ExitCode;

                if (ECode == 0)
                {
                    Console.WriteLine("PASS: Installation of SQL Server 2016 Express has completed successfully.");
                    Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    MessageBox.Show("SQL Server 2016 Express has been installed successfully.", "Installation Complete");
                }
                else if (ECode == 3010)
                {
                    Console.WriteLine("PASS: Installation of SQL Server 2016 Express has completed successfully.");
                    Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    MessageBox.Show("SQL Server 2016 Express has been installed successfully.", "Installation Complete");
                    goto Cleanup;
                }
                else if (ECode == 1)
                {
                    Console.WriteLine("FAIL: Installation of SQL Server 2016 Express has been terminated forcefully.");
                    Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    MessageBox.Show("The SQL Server 2016 Installer process has been terminated.", "Installation Failure");
                    goto Cleanup;
                }
                else if (ECode == 1602)
                {
                    Console.WriteLine("FAIL: Installation of SQL Server 2016 Express has been cancelled.");
                    Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    MessageBox.Show("The Installation of SQL Server 2016 Express has been cancelled.", "Installation Failure");
                    goto Cleanup;
                }
                else if (ECode == -2067529717)
                {
                    Console.WriteLine("FAIL: Installation of SQL Server 2016 Express has failed, multiple instances of the installer are running." + Environment.NewLine + "This installer will exit silently.");
                    Console.WriteLine("EXIT CODE: " + ECode.ToString());
                    goto Cleanup;
                }
                else // Not a standard exit code.
                {
                    Console.WriteLine("FAIL: Installation has failed." + Environment.NewLine + "EXIT CODE: " + ECode.ToString() + ".");
                    DialogResult DR2 = MessageBox.Show("SQL Server 2016 has failed to Install." + Environment.NewLine + "Exit Code: " + ECode, "Installation Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    if (DR2 == DialogResult.OK) // Display the installation Summary.txt to the user if it exists.
                    try
                    {
                        string Logfile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Microsoft SQL Server\\130\\Setup Bootstrap\\Log\\Summary.txt";
                        if (File.Exists(Logfile) == true)
                        {
                            Process.Start("notepad.exe", Logfile);
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Couldn't produce log to the user, swallowing exception.");
                    }
                    goto Cleanup;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("FAIL: The following error has occured while trying to launch the SQL Install:");
                MessageBox.Show("SQL Server 2016 has failed to launch." + Environment.NewLine + "Error: " + e.Message, "Installation Failure");
                Console.WriteLine("ERROR: " + e.Message);
            }
            


            // Exit

            Cleanup:
            Console.WriteLine(Environment.NewLine + "Exiting...");
            Environment.Exit(0);
        }


        // Check if .NET 4.0 is installed.

        private static bool IsDotNet4Installed()
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client");
            if (registryKey == null)
                return false;
            object obj = registryKey.GetValue("Install");
            if (obj != null)
                return obj.ToString() == "1";
            else
                return false;
        }
    }
}

