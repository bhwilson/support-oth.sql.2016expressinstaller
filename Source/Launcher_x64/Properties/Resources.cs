﻿// Type: Cons.Gateway.SqlInstall.Properties.Resources
// Assembly: Setup, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 45D3FF47-EDBF-4644-B0FD-BCFE30F72DC6
// Assembly location: D:\Software\Projects\SQL 2012 Builder\Setup.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Cons.Gateway.SqlInstall.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Resources.resourceMan, (object) null))
          Resources.resourceMan = new ResourceManager("Cons.Gateway.SqlInstall.Properties.Resources", typeof (Resources).Assembly);
        return Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return Resources.resourceCulture;
      }
      set
      {
        Resources.resourceCulture = value;
      }
    }

    internal static string Program_Main_Console_SQL_Installer
    {
      get
      {
        return Resources.ResourceManager.GetString("Program_Main_Console_SQL_Installer", Resources.resourceCulture);
      }
    }

    internal Resources()
    {
    }
  }
}
