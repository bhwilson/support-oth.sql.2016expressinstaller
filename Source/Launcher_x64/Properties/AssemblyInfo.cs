﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Launcher_x64")]
[assembly: AssemblyDescription("SQL Installer Launcher")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("OnTheHouse")]
[assembly: AssemblyProduct("SQL 2014 Express Installer for Gateway Live")]
[assembly: AssemblyCopyright("Copyright © OnTheHouse 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4462c5c4-17b8-4eb9-980b-856c9c52bef7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
